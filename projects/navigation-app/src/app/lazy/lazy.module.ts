import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LazyComponent } from './lazy.component';
import { RouterModule } from '@angular/router';
import { PollingLibModule } from 'polling-lib';



@NgModule({
  declarations: [
    LazyComponent
  ],
  imports: [
    CommonModule,
    PollingLibModule,
    RouterModule.forChild([
      {path: '', component: LazyComponent}
    ])
  ],
  exports: [RouterModule]
})
export class LazyModule { }
