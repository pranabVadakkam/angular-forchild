import { Component, OnInit } from '@angular/core';
import { PollingLibService } from 'polling-lib';

@Component({
  selector: 'app-lazy',
  templateUrl: './lazy.component.html',
  styleUrls: ['./lazy.component.scss']
})
export class LazyComponent implements OnInit {

  constructor(public pollingService: PollingLibService) { }

  ngOnInit(): void {
  }

}
