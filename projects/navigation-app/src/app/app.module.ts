import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { PollingLibModule } from 'polling-lib';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeModule } from './home/home.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HomeModule,
    AppRoutingModule,
    PollingLibModule.forRoot({interval: 3000})
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
