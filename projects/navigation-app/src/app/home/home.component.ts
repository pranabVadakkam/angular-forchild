import { Component, OnInit } from '@angular/core';
import { PollingLibService } from 'polling-lib';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(public pollingService: PollingLibService) { }

  ngOnInit(): void {
  }

}
