import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PollingLibComponent } from './polling-lib.component';

describe('PollingLibComponent', () => {
  let component: PollingLibComponent;
  let fixture: ComponentFixture<PollingLibComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PollingLibComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PollingLibComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
