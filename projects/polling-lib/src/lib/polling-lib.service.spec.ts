import { TestBed } from '@angular/core/testing';

import { PollingLibService } from './polling-lib.service';

describe('PollingLibService', () => {
  let service: PollingLibService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PollingLibService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
