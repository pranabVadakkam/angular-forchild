import { Inject, Injectable, InjectionToken } from '@angular/core';
import { timer } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

export const INTERVAL = new InjectionToken<number>('');
@Injectable()
export class PollingLibService {
  polling$ = timer(0, this.interval).pipe(shareReplay());
  constructor(@Inject(INTERVAL) private interval: number) {
  }
}
