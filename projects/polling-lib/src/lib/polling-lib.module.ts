import { ModuleWithProviders, NgModule } from '@angular/core';
import { PollingLibService, INTERVAL } from './polling-lib.service';
import { PollingLibComponent } from './polling-lib.component';


@NgModule({
  declarations: [
    PollingLibComponent
  ],
  imports: [
  ],
  exports: [
    PollingLibComponent
  ]
})
export class PollingLibModule {
  static forRoot(config: any): ModuleWithProviders<PollingLibModule> {
    return {
      ngModule: PollingLibModule,
      providers: [PollingLibService, {provide: INTERVAL, useValue: config['interval']}]
    }
  }
}

